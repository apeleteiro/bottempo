<p align="center"><img height="188" width="198" src="https://peleteiro.eu/bottempo.png"></p>
<h1 align="center">Bottempo</h1>

*Se chove, que chova!*

## About Bottempo

[Bottempo](https://bottempo.gal/) is a Telegram bot that allows you to ask about the weather anywhere in Galicia as well as to configure periodic alerts or by phenomenon to receive notifications, using the [MeteoSIX](https://www.meteogalicia.gal/web/proxectos/meteosix.action?request_locale=gl) API, maintained by the Government of Galicia.

## Components
**Two are the main components used for its implementation**

- Laravel: [Link](https://laravel.com/)
- BotMan: [Link](https://botman.io/)

## Security Vulnerabilities

If you discover a security vulnerability, please send an e-mail to Antonio Peleteiro at apeleteiro@gmail.com. All security vulnerabilities will be promptly addressed.

## License

Bottempo is free software distributed under the terms of the MIT license.

