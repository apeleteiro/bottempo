<?php

namespace App\Console;

use App\Services\PeriodicAlertService;
use App\Services\PhenomenonAlertService;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $periodicAlertService = new PeriodicAlertService();
            $periodicAlertService->launchAlerts();
        })->everyMinute();

        $schedule->call(function () {
            $phenomenonAlertService = new PhenomenonAlertService();
            $phenomenonAlertService->launchAlerts();
        })->hourly();

        $schedule->command('cache:clear')->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
