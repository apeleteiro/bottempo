<?php

namespace App\Conversations;

use App\Models\PeriodicAlert;
use App\Models\Phenomenon;
use App\Models\PhenomenonAlert;
use App\Models\User;
use App\Services\DeleteAlertService;
use App\Services\GetAlertsService;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class AlertsConversation extends Conversation
{
    const COMMAND = '/alerts';

    /**
     * @var BotMan
     */
    protected $bot;

    protected $getAlertsService;
    protected $deleteAlertService;
    protected $locale;

    /**
     * StartConversation constructor.
     *
     * @param BotMan $bot
     */
    public function __construct(BotMan $bot)
    {
        $this->bot = $bot;
        $this->getAlertsService = new GetAlertsService();
        $this->deleteAlertService = new DeleteAlertService();
        $this->locale = User::where('telegram_id', $this->bot->getMessage()->getSender())->first()->locale;
    }

    /**
     * Start the conversation.
     *
     * @return mixed|void
     */
    public function run()
    {
        $translation = __('messages.alertsType', [], $this->locale);
        $question = Question::create($translation)
            ->addButtons([
                Button::create(trans_choice('messages.periodicAlert', 2, [], $this->locale))
                      ->value(PeriodicAlert::VALUE),
                Button::create(trans_choice('messages.phenomenonAlert', 2, [], $this->locale))
                      ->value(PhenomenonAlert::VALUE)

            ]);

        $this->ask($question, function ($answer) {
            if ($answer->isInteractiveMessageReply()) {
                $alerts = $this->getAlertsService->getAlerts($answer->getValue(), $this->bot->getMessage()->getSender());
                $this->listAlerts($answer, $alerts);
            } else {
                $this->repeat();
            }
        });
    }

    private function listAlerts($answer, $alerts)
    {
        $alertTypeKey = $answer->getValue() === PeriodicAlert::VALUE
            ? 'messages.periodicAlert'
            : 'messages.phenomenonAlert';

        if (count($alerts) === 0) {
            $this->say(__('messages.noAlerts', [
                'alertType' => trans_choice($alertTypeKey, 1, [], $this->locale)
            ], $this->locale));
        }

        foreach ($alerts as $alert) {
            if ($answer->getValue() === PeriodicAlert::VALUE) {
                $time = \DateTime::createFromFormat('Y-m-d H:i:s', $alert->schedule)->format('H:i');
                $question = trans_choice($alertTypeKey, 1, [], $this->locale) .': '. $alert->location .' - '. $time;
                $this->deleteAlert($question, $alert, PeriodicAlert::VALUE);
            }
            if ($answer->getValue() === PhenomenonAlert::VALUE) {
                $phenomenon = Phenomenon::find($alert->phenomenon_id);
                $question = trans_choice($alertTypeKey, 1, [], $this->locale) .': '. $alert->location .' - '.
                            __('weather.' . $phenomenon->type, [], $this->locale);
                $this->deleteAlert($question, $alert, PhenomenonAlert::VALUE);
            }
        }
    }

    private function deleteAlert($questionText, $alert, $alertType)
    {
        $question = Question::create($questionText)
            ->addButtons([Button::create(__('messages.deleteButton', [], $this->locale))
            ->value($alertType.'|'.$alert->id)]);
        $this->ask($question, function ($answer) {
            if ($answer->isInteractiveMessageReply()) {
                $this->deleteAlertService->delete($answer->getValue());
                $this->say(__('messages.deleteAlert', [], $this->locale));
            } else {
                $this->repeat();
            }
        });
    }
}
