<?php

namespace App\Conversations;

use App\Models\PeriodicAlert;
use App\Models\Phenomenon;
use App\Models\PhenomenonAlert;
use App\Models\User;
use App\Services\PeriodicAlertService;
use App\Services\PhenomenonAlertService;
use App\Services\WeatherService;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;
use Illuminate\Support\Facades\Log;

class CreateAlertConversation extends Conversation
{
    protected $alertType;
    protected $sender;
    protected $periodicAlertService;
    protected $phenomenonAlertService;
    protected $location;
    protected $schedule;
    protected $phenomenon;
    protected $locale;

    public function __construct($alertType, $sender)
    {
        $this->alertType = $alertType;
        $this->sender = $sender;
        $this->periodicAlertService = new PeriodicAlertService();
        $this->phenomenonAlertService = new PhenomenonAlertService();
        $this->locale = User::where('telegram_id', $sender)->first()->locale;
    }

    /**
     * Start the conversation
     */
    public function run()
    {
        Log::debug('Debug::CreateAlertConversation::run ' . $this->alertType);

        $this->askLocation();
    }

    private function askLocation()
    {
        $translation = __('messages.askLocation', [], $this->locale);
        $this->ask($translation, function(Answer $answer) {
            if (!$this->validateLocationAnswer($answer)) {
                $this->say(__('messages.errorLocation', [], $this->locale));
                return $this->repeat();
            }
            $this->location = $answer->getText();
            Log::debug('Debug::CreateAlertConversation::askLocation: ' . $this->location);

            if ($this->alertType === PeriodicAlert::VALUE) {
                $this->askSchedule();
            }

            if ($this->alertType === PhenomenonAlert::VALUE) {
                $this->askPhenomenon();
            }
        });
    }

    private function askSchedule()
    {
        $translation = __('messages.askSchedule', [], $this->locale);
        $this->ask($translation, function(Answer $answer) {
            if (!$this->validateScheduleAnswer($answer)) {
                $this->say(__('messages.errorSchedule', [], $this->locale));
                return $this->repeat();
            }
            $this->schedule = $answer->getText();
            Log::debug('Debug::CreateAlertConversation::askSchedule: ' . $this->schedule);

            $this->periodicAlertService->create($this->sender, $this->location, $this->schedule);
            $translation = __('messages.periodicAlertCreated', [
                'place' => $this->location,
                'time' => $this->schedule
            ], $this->locale);
            $this->say($translation);
        });
    }

    private function askPhenomenon()
    {
        $translation = __('messages.askPhenomenon', [], $this->locale);
        $question = Question::create($translation)
            ->addButtons([
                Button::create(__('messages.rain', [], $this->locale))->value(PhenomenonAlert::RAIN),
                Button::create(__('messages.snow', [], $this->locale))->value(PhenomenonAlert::SNOW),
                Button::create(__('messages.fog', [], $this->locale))->value(PhenomenonAlert::FOG)
            ]);
        $this->ask($question, function(Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                $this->phenomenon = Phenomenon::find($answer->getText());
                Log::debug('Debug::CreateAlertConversation::askPhenomenon: ' . $this->phenomenon->type);

                $this->phenomenonAlertService->create($this->sender, $this->location, $this->phenomenon->id);
                $translation = __('messages.phenomenonAlertCreated', [
                    'place' => $this->location,
                    'phenomenon' => __('weather.'.$this->phenomenon->type, [], $this->locale)
                ], $this->locale);
                $this->say($translation);
            } else {
                $this->repeat();
            }
        });
    }

    private function validateLocationAnswer(Answer $answer): bool
    {
        try {
            WeatherService::getWeather($answer->getText());
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    private function validateScheduleAnswer(Answer $answer): bool
    {
        $regex = '#([01]?[0-9]|2[0-3]):[0-5][0-9]#';
        if (!preg_match($regex, $answer->getText())) {
            return false;
        }
        return true;
    }
}
