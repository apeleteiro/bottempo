<?php

namespace App\Conversations;

use App\Models\PeriodicAlert;
use App\Models\PhenomenonAlert;
use App\Models\User;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class NewAlertConversation extends Conversation
{
    const COMMAND = '/newalert';

    /**
     * @var BotMan
     */
    protected $bot;

    protected $locale;

    /**
     * StartConversation constructor.
     *
     * @param BotMan $bot
     */
    public function __construct(BotMan $bot)
    {
        $this->bot = $bot;
        $this->locale = User::where('telegram_id', $this->bot->getMessage()->getSender())->first()->locale;
    }

    /**
     * Start the conversation.
     *
     * @return mixed|void
     */
    public function run()
    {
        $translation = __('messages.newAlert', [], $this->locale);
        $question = Question::create($translation)
            ->addButtons([
                Button::create(trans_choice('messages.periodicAlert', 1, [], $this->locale))
                      ->value(PeriodicAlert::VALUE),
                Button::create(trans_choice('messages.phenomenonAlert',1, [], $this->locale))
                      ->value(PhenomenonAlert::VALUE)
            ]);

        $this->ask($question, function ($answer) {
            if ($answer->isInteractiveMessageReply()) {
                $this->bot->startConversation(new CreateAlertConversation($answer->getValue(), $this->bot->getMessage()->getSender()));
            } else {
                $this->repeat();
            }
        });
    }
}
