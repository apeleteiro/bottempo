<?php

namespace App\Conversations;

use App\Models\PeriodicAlert;
use App\Models\PhenomenonAlert;
use App\Models\User;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class StartConversation extends Conversation
{
    const COMMAND = '/start';

    /**
     * @var BotMan
     */
    protected $bot;

    protected $locale;

    /**
     * StartConversation constructor.
     *
     * @param BotMan $bot
     */
    public function __construct(BotMan $bot)
    {
        $this->bot = $bot;
        $this->locale = substr(User::GALICIAN_LOCALE, 0, -1);
    }

    /**
     * Start the conversation.
     *
     * @return mixed|void
     */
    public function run()
    {
        $this->say(__('messages.welcome1', [], $this->locale));
        $translation = __('messages.welcome2', [], $this->locale);
        $question = Question::create($translation)
            ->addButtons([
                Button::create(trans_choice('messages.galician', 1, [], $this->locale))
                      ->value(User::GALICIAN_LOCALE),
                Button::create(trans_choice('messages.spanish',1, [], $this->locale))
                      ->value(User::SPANISH_LOCALE),
                Button::create(trans_choice('messages.english',1, [], $this->locale))
                      ->value(User::ENGLISH_LOCALE)
            ]);

        $this->ask($question, function ($answer) {
            if ($answer->isInteractiveMessageReply()) {
                $this->locale = substr($answer->getValue(), 0, -1);
                $this->registerUser();
                $this->continueConversation();
            } else {
                $this->repeat();
            }
        });
    }

    private function registerUser()
    {
        $user = User::where('telegram_id', $this->bot->getMessage()->getSender())->firstOr(function () {
            $user = new User;
            $user->telegram_id = $this->bot->getMessage()->getSender();
            $user->locale = $this->locale;
            $user->save();
            $this->locale = $user->locale;
        });
        if (!is_null($user)) {
            $user->locale = $this->locale;
            $user->update();
            $this->locale = $user->locale;
        }
    }

    private function continueConversation()
    {
        $this->say(__('messages.setLanguage1', [], $this->locale));
        $this->say(__('messages.setLanguage2', [], $this->locale));
        $translation = __('messages.setLanguage3', [], $this->locale);
        $question = Question::create($translation)
            ->addButtons([
                Button::create(trans_choice('messages.periodicAlert', 1, [], $this->locale))
                      ->value(PeriodicAlert::VALUE),
                Button::create(trans_choice('messages.phenomenonAlert',1, [], $this->locale))
                      ->value(PhenomenonAlert::VALUE)
            ]);

        $this->ask($question, function ($answer) {
            if ($answer->isInteractiveMessageReply()) {
                $this->bot->startConversation(
                    new CreateAlertConversation($answer->getValue(), $this->bot->getMessage()->getSender())
                );
            }
        });
    }
}
