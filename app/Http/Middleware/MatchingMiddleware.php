<?php

namespace App\Http\Middleware;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\Interfaces\Middleware\Matching;
use BotMan\BotMan\Messages\Incoming\IncomingMessage;

class MatchingMiddleware implements Matching
{
    const ADMIN_ID = 9999999;
    /**
     * @param IncomingMessage $message
     * @param string $pattern
     * @param bool $regexMatched Indicator if the regular expression was matched too
     * @return bool
     */
    public function matching(IncomingMessage $message, $pattern, $regexMatched)
    {
        //$isAdministrator = $message->getSender() === self::ADMIN_ID;
        $isAdministrator = 9999999 === self::ADMIN_ID;
        return $isAdministrator && $regexMatched;
    }
}
