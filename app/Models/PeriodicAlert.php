<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PeriodicAlert extends Model
{
    const VALUE = 'periodic';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'alerts_periodic';
}
