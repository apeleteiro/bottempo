<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhenomenonAlert extends Model
{
    const VALUE = 'phenomenon';

    const FOG = 6;
    const RAIN = 11;
    const SNOW = 12;

    public static $FOG_ARRAY = [6, 14, 15];
    public static $RAIN_ARRAY = [7, 8, 10, 11, 13, 17, 18, 19, 21];
    public static $SNOW_ARRAY = [9, 12, 20];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'alerts_phenomenon';
}
