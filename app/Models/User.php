<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    const GALICIAN_LOCALE = 'glg';
    const SPANISH_LOCALE = 'esp';
    const ENGLISH_LOCALE = 'eng';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';
}
