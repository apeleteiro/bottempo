<?php

namespace App\Services;

use App\Models\PeriodicAlert;
use App\Models\PhenomenonAlert;
use App\Models\User;

class DeleteAlertService
{
    public function delete($answer)
    {
        list($alertType, $alertId) = explode('|', $answer);

        switch ($alertType) {
            case PeriodicAlert::VALUE:
                $alert = PeriodicAlert::find($alertId);
                $alert->delete();
                break;
            case PhenomenonAlert::VALUE:
                $alert = PhenomenonAlert::find($alertId);
                $alert->delete();
                break;
        }
    }
}
