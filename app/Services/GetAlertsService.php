<?php

namespace App\Services;

use App\Models\PeriodicAlert;
use App\Models\PhenomenonAlert;
use App\Models\User;

class GetAlertsService
{
    public function getAlerts($alertsType, $sender)
    {
        $user = User::where('telegram_id', $sender)->first();

        switch ($alertsType) {
            case PeriodicAlert::VALUE:
                $alerts = PeriodicAlert::where('user_id', $user->id)->get();
                break;
            case PhenomenonAlert::VALUE:
                $alerts = PhenomenonAlert::where('user_id', $user->id)->get();
                break;
            default:
                $alerts = [];
        }

        return $alerts;
    }
}
