<?php

namespace App\Services;

use App\Models\PeriodicAlert;
use App\Models\User;
use BotMan\Drivers\Telegram\TelegramDriver;

class PeriodicAlertService
{
    public function create($sender, $location, $schedule)
    {
        $user = User::where('telegram_id', $sender)->first();
        $time = \DateTime::createFromFormat('H:i', $schedule);

        $periodicAlert = new PeriodicAlert;
        $periodicAlert->location = $location;
        $periodicAlert->user_id = $user->id;
        $periodicAlert->schedule = $time;
        $periodicAlert->save();
    }

    public function launchAlerts()
    {
        $botman = resolve('botman');
        $periodicAlerts = PeriodicAlert::all();
        foreach ($periodicAlerts as $periodicAlert) {
            $time = \DateTime::createFromFormat('Y-m-d H:i:s', $periodicAlert->schedule)->format('H:i');
            date_default_timezone_set('Europe/Madrid');
            $now = date('H:i');

            if ($time === $now) {
                $user = User::find($periodicAlert->user_id);
                $response = WeatherService::getWeather($periodicAlert->location);
                $message = __('messages.launchPeriodicAlert', [
                    'place' => $response['place'],
                    'weather' => __('weather.' . $response['forecast'], [], $user->locale),
                    'temperature' => $response['temperature']
                ], $user->locale);
                $botman->say($message, $user->telegram_id, TelegramDriver::class);
            }
        }
    }
}
