<?php

namespace App\Services;

use App\Models\Phenomenon;
use App\Models\PhenomenonAlert;
use App\Models\User;
use BotMan\Drivers\Telegram\TelegramDriver;

class PhenomenonAlertService
{
    public function create($sender, $location, $phenomenonId)
    {
        $user = User::where('telegram_id', $sender)->first();

        $periodicAlert = new PhenomenonAlert();
        $periodicAlert->location = $location;
        $periodicAlert->user_id = $user->id;
        $periodicAlert->phenomenon_id = $phenomenonId;
        $periodicAlert->save();
    }

    public function launchAlerts()
    {
        $phenomenonAlerts = PhenomenonAlert::all();
        foreach ($phenomenonAlerts as $phenomenonAlert) {
            $response = WeatherService::getWeather($phenomenonAlert->location);
            $responsePhenomenon = Phenomenon::where('type', $response['forecast'])->first();

            if ($phenomenonAlert->phenomenon_id === PhenomenonAlert::FOG
                && in_array($responsePhenomenon->id, PhenomenonAlert::$FOG_ARRAY)) {
                $this->notifyUser($phenomenonAlert, $response);
            }

            if ($phenomenonAlert->phenomenon_id === PhenomenonAlert::RAIN
                && in_array($responsePhenomenon->id, PhenomenonAlert::$RAIN_ARRAY)) {
                $this->notifyUser($phenomenonAlert, $response);
            }

            if ($phenomenonAlert->phenomenon_id === PhenomenonAlert::SNOW
                && in_array($responsePhenomenon->id, PhenomenonAlert::$SNOW_ARRAY)) {
                $this->notifyUser($phenomenonAlert, $response);
            }
        }
    }

    private function notifyUser($phenomenonAlert, $response)
    {
        $botman = resolve('botman');
        $user = User::find($phenomenonAlert->user_id);
        $message = __('messages.launchPhenomenonAlert', [
            'place' => $response['place'],
            'weather' => __('weather.' . $response['forecast'], [], $user->locale),
            'temperature' => $response['temperature']
        ], $user->locale);
        $botman->say($message, $user->telegram_id, TelegramDriver::class);
    }
}
