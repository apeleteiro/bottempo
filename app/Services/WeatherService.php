<?php

namespace App\Services;

class WeatherService
{
    const URL_FIND = 'https://servizos.meteogalicia.gal/apiv3/findPlaces?location=';
    const URL_INFO = 'https://servizos.meteogalicia.gal/apiv3/getNumericForecastInfo?locationIds=';
    const URL_KEY = '&API_KEY=';

    /**
     * @param $location
     *
     * @return array
     */
    public static function getWeather($location)
    {
        $urlFind = self::URL_FIND . urlencode($location) . self::URL_KEY . env('METEOGALICIA_API_KEY');
        $responseFind = json_decode(file_get_contents($urlFind));

        $locationId = $responseFind->features[0]->properties->id;

        $urlForecastInfo = self::URL_INFO. urlencode($locationId). self::URL_KEY . env('METEOGALICIA_API_KEY').'';
        $responseForecastInfo = json_decode(file_get_contents($urlForecastInfo));

        return [
            'place' => $responseFind->features[0]->properties->name,
            'forecast' => $responseForecastInfo->features[0]->properties->days[0]->variables[0]->values[0]->value,
            'temperature' => $responseForecastInfo->features[0]->properties->days[0]->variables[1]->values[0]->value
        ];
    }
}
