<?php

use App\Models\Phenomenon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Phenomenon::create(['type' => 'SUNNY']);
        Phenomenon::create(['type' => 'HIGH_CLOUDS']);
        Phenomenon::create(['type' => 'PARTLY_CLOUDY']);
        Phenomenon::create(['type' => 'OVERCAST']);
        Phenomenon::create(['type' => 'CLOUDY']);
        Phenomenon::create(['type' => 'FOG']);
        Phenomenon::create(['type' => 'SHOWERS']);
        Phenomenon::create(['type' => 'OVERCAST_AND_SHOWERS']);
        Phenomenon::create(['type' => 'INTERMITENT_SNOW']);
        Phenomenon::create(['type' => 'DRIZZLE']);
        Phenomenon::create(['type' => 'RAIN']);
        Phenomenon::create(['type' => 'SNOW']);
        Phenomenon::create(['type' => 'STORMS']);
        Phenomenon::create(['type' => 'MIST']);
        Phenomenon::create(['type' => 'FOG_BANK']);
        Phenomenon::create(['type' => 'MID_CLOUDS']);
        Phenomenon::create(['type' => 'WEAK_RAIN']);
        Phenomenon::create(['type' => 'WEAK_SHOWERS']);
        Phenomenon::create(['type' => 'STORM_THEN_CLOUDY']);
        Phenomenon::create(['type' => 'MELTED_SNOW']);
        Phenomenon::create(['type' => 'RAIN_HAIL']);
    }
}
