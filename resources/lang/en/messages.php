<?php

return [
    'welcome1' => 'Welcome to Bottempo!',
    'welcome2' => 'Please select your language:',
    'periodicAlert' => 'Periodic alert|Periodic alerts',
    'phenomenonAlert' => 'Phenomenon alert|Phenomenon alerts',
    'weatherResponse' => 'The weather in :place is: :weather with :temperature ºC.',
    'noResults1' => 'This place does not return results, please try again.',
    'noResults2' => 'Remember that you can only search for places in Galicia.',
    'askLocation' => 'For which place you want to create the alert? (Example: Vigo)',
    'askSchedule' => 'For what time do you want to schedule the alert? (Example: 18:00)',
    'periodicAlertCreated' => 'Periodic alert for :place created at :time every day.',
    'askPhenomenon' => 'For what phenomenon do you want to configure the alert?',
    'phenomenonAlertCreated' => ':phenomenon phenomenon alert created for :place',
    'alertsType' => 'What type of alerts do you want to list?',
    'help1' => 'You can ask for the weather anywhere in Galicia ("How is the weather in Ourense?").',
    'help2' => 'Or type /alerts for manage your alerts.',
    'help3' => 'You can also configure new alerts using /newalert or selecting the type below:',
    'noAlerts' => 'You don\'t have any :alertType configured.',
    'deleteButton' => 'Delete',
    'deleteAlert' => 'Alert successfully removed.',
    'launchPeriodicAlert' => 'Periodic alert: :weather in :place with :temperature ºC.',
    'launchPhenomenonAlert' => 'Phenomenon alert: :weather in :place with :temperature ºC.',
    'rain' => 'Rain',
    'snow' => 'Snow',
    'fog' => 'Fog',
    'newAlert' => 'What type of alert do you want to configure?',
    'galician' => 'Galician',
    'spanish' => 'Spanish',
    'english' => 'English',
    'setLanguage1' => 'Language selected correctly.',
    'setLanguage2' => 'You can now ask for the weather anywhere in Galicia ("How is the weather in Ourense?").',
    'setLanguage3' => 'You can also configure new alerts using /newalert or selecting the type below:',
    'errorLocation' => 'Galician place not found.',
    'errorSchedule' => 'Incorrect time format.',
    'fallback' => 'Wrong instruction, type /help to get help.',
];
