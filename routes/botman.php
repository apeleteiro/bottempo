<?php

use App\Conversations\AlertsConversation;
use App\Conversations\HelpConversation;
use App\Conversations\NewAlertConversation;
use App\Conversations\StartConversation;
use App\Http\Middleware\DialogflowV2;
use App\Models\User;
use App\Services\WeatherService;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;
use Illuminate\Support\Facades\Log;

$botman = resolve('botman');
$dialogflow = DialogflowV2::create()->listenForAction();
$botman->middleware->received($dialogflow);

$botman->fallback(function ($bot) {
    $locale = User::where('telegram_id', $bot->getMessage()->getSender())->first()->locale
              ?? substr(User::GALICIAN_LOCALE, 0, -1);
    $bot->reply(__('messages.fallback', [], $locale));
    $message = $bot->getMessage();
    Log::debug('Debug::fallback: ' . $message->getText());
});

$botman->hears('weather-search', function ($bot) {
    $locale = User::where('telegram_id', $bot->getMessage()->getSender())->first()->locale
              ?? substr(User::GALICIAN_LOCALE, 0, -1);
    try {
        $extras = $bot->getMessage()->getExtras();
        $location = explode(',', $extras['apiParameters']['geo-city'])[0];
        Log::debug('Debug::weather-search: ' . $location);

        $response = WeatherService::getWeather($location);

        $translation = __('messages.weatherResponse', [
            'place' => $response['place'],
            'weather' => __('weather.' . $response['forecast'], [], $locale),
            'temperature' => $response['temperature']
        ], $locale);
        $message = OutgoingMessage::create($translation);

        $bot->reply($message);
    } catch (Exception $exception) {
        Log::error('Debug::weather-search::Exception: ' . $exception->getMessage());
        $bot->reply(__('messages.noResults1', [], $locale));
        $bot->reply(__('messages.noResults2', [], $locale));
    }
})->middleware($dialogflow)->skipsConversation();

$botman->hears(StartConversation::COMMAND, function ($bot) {
    Log::debug('Debug::Command: ' . StartConversation::COMMAND);
    $bot->startConversation(new StartConversation($bot));
})->stopsConversation();

$botman->hears(AlertsConversation::COMMAND, function ($bot) {
    Log::debug('Debug::Command: ' . AlertsConversation::COMMAND);
    $bot->startConversation(new AlertsConversation($bot));
})->stopsConversation();

$botman->hears(HelpConversation::COMMAND, function ($bot) {
    Log::debug('Debug::Command: ' . HelpConversation::COMMAND);
    $bot->startConversation(new HelpConversation($bot));
})->stopsConversation();

$botman->hears(NewAlertConversation::COMMAND, function ($bot) {
    Log::debug('Debug::Command: ' . NewAlertConversation::COMMAND);
    $bot->startConversation(new NewAlertConversation($bot));
})->stopsConversation();
